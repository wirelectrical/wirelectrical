const data = [
	{ Prophecy: "Is 7:1-4", Fulfilment: "Mt 1:18-23", Event: "Virgin Birth"},
	{ Prophecy: "Hos 11:1", Fulfilment: "Mt 2:1-15", Event: "Flee to Egypt"},
	{ Prophecy: "Mic 5:2", Fulfilment: "Lk 2:1-7", Event: "Born in Bethlehem"},
	{ Prophecy: "Is 9:1-2", Fulfilment: "Mt 4:12-17", Event: "Preach at Galilee"},
	{ Prophecy: "Zec 11:12", Fulfilment: "Mt 26:14-16", Event: "30 Silver Coins"},
	{ Prophecy: "Mal 3:1", Fulfilment: "Mt 3:13-16", Event: "Messenger Who Prepares the Way"},
	{ Prophecy: "Psm 69:21", Fulfilment: "Jn 19:28-29", Event: "Drink Vinegar"},
	{ Prophecy: "Jer 31:27", Fulfilment: "Mt 13:24-29", Event: "Sowing 2 Kinds of Seeds"},
	{ Prophecy: "Num 21:9", Fulfilment: "Jn 3:14-18", Event: "Jesus is Bronze Snake"},
	{ Prophecy: "Psm 22:1", Fulfilment: "Mt 27:46", Event: "Why You have Forsaken Me?"},
	{ Prophecy: "Psm 41:9", Fulfilment: "Jn 13:21", Event: "Betrayed by Judas"},
	{ Prophecy: "Psm 78:2", Fulfilment: "Mt 13:34-35", Event: "Jesus Speaks in Parables"},
	{ Prophecy: "Is 61:1-2", Fulfilment: "Lk 4:16-21", Event: "Isaiah"},
	{ Prophecy: "Exo 12:21-27", Fulfilment: "1 Cor 5:7", Event: "Jesus is Passover Lamb"},
	{ Prophecy: "Jer 31:31", Fulfilment: "Lk 22:14-20", Event: "Making a New Covenant"},
	{ Prophecy: "Zec 9:9", Fulfilment: "Mt 21:1-11", Event: "Riding on a Foal of a Donkey"},
	{ Prophecy: "Psm 22:18", Fulfilment: "Jn 19:23-24", Event: "Casting Lot for Jesus' Clothes"},
	{ Prophecy: "Mt 13:30", Fulfilment: "Rev 14:14-16", Event: "Harvesting"},
	{ Prophecy: "Is 28:16", Fulfilment: "1 Pt 2:4-7", Event: "Jesus is Corner Stone"},
	{ Prophecy: "Psm 34:20", Fulfilment: "Jn 19:31-34", Event: "Jesus' Bone was Not Broken"}
];

let rand = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
let haveIt = [];
let states = [];
let capitals = [];
let scapitals = [];
let sa = null;
let lines = [];
let lined = { x1: null, y1: null, x2: null, y2: null };
let toggle = false;
let onbox = false;
let score = 0;
let flag = true;

// All p5 sketches need a setup function, most of it pretty self-explano

function setup() {
	width = 800;
	height = 380;
	let rv = [];
	let cnv = createCanvas(width, height);

	border();
	for (var i = 0; i < 5; i++) {
		rv[i] = generateUniqueRandom(20);
	}
	
	scapitals = rv.map(x => data[x].Fulfilment);
	//data.map((e, index) => {
	//	scapitals.push(e.Fulfilment);
	//});
	//shuffling the capitals
	scapitals = shuffle(scapitals);

	//displaying the boxes
	rv.map((e, index) => {
		states.push(new Prophecy(0, index * 75 + 20, data[e].Event + " - " + data[e].Prophecy, data[e].Fulfilment));
		capitals.push(new Fulfilment(700, index * 75 + 20, scapitals[index]));
	});
}

function generateUniqueRandom(maxNr) {
    //Generate random number
    let random = (Math.random() * maxNr).toFixed();
    //Coerce to number by boxing
    random = Number(random);

    if(!haveIt.includes(random)) {
        haveIt.push(random);
        return random;
    } else {
        if(haveIt.length < maxNr) {
          //Recursively generate number
         return  generateUniqueRandom(maxNr);
        } else {
          console.log('No more numbers available.')
          return false;
        }
    }
}

//clears the screen between renderings
function border() {
	strokeWeight(1);
	stroke(0);
    background(93, 94, 100)
}

// Processing clicks
function mouseClicked() {
	if (!toggle) {
		lined.x1 = mouseX;
		lined.y1 = mouseY;
	} else {
		lined.x2 = mouseX;
		lined.y2 = mouseY;

		for (let i = 0; i < states.length; i++) {
			if (states[i].selected) {
				for (let j = 0; j < capitals.length; j++) {
					if (capitals[j].hover(mouseX, mouseY)) {
						//pushing lines drawn into an array for rendering
                        //window.alert(lined.x1 + ", " + lined.y1+", "+lined.x2+", "+lined.y2)
						lines.push(new Connect(lined.x1, lined.y1, lined.x2, lined.y2));
						//reseting line-holder
						lined = { x1: null, y1: null, x2: null, y2: null };
						break;
					}
				}
			}
		}
	}

	toggle = !toggle;

	for (let i = 0; i < states.length; i++) {
		//processing clicks on states
		states[i].click(mouseX, mouseY);
	}

	for (let i = 0; i < capitals.length; i++) {
		//processing clicks on capitals
		capitals[i].click(mouseX, mouseY);
	}
}

//where the magic happens
function draw() {
	border();

	for (let i = 0; i < states.length; i++) {
		states[i].show(mouseX, mouseY);

		if (states[i].selected) {
			strokeWeight(10);
			stroke(30, 200, 190);

			line(
				states[i].x + states[i].w,
				states[i].y + states[i].h / 2,
				mouseX,
				mouseY
			);

			fill(255, 0, 0);
			strokeWeight(2);
			circle(states[i].x + states[i].w, states[i].y + states[i].h / 2, 10);
		}
        
	}

	for (let i = 0; i < capitals.length; i++) {
		strokeWeight(1);
		stroke(0);
		capitals[i].show(mouseX, mouseY);
	}

	for (let i = 0; i < lines.length; i++) {
		let l = new Connect(lines[i].x, lines[i].y, lines[i].xx, lines[i].yy);
		l.show(l.x, l.y, l.xx, l.yy);
	}

	if(score >= 5 && flag){
		window.alert("Great job!!! The efficiency of the spaceship has increased a little. Thank you for overcoming. The sabotage spirit has moved on to other places in the ship, go around and complete the tasks. Keep on overcoming!");
		flag = false;
		window.close();
	}
}

//A class for connecting lines joining each pair
class Connect {
	constructor(x, y, xx, yy) {
		this.x = x;
		this.y = y;
		this.xx = xx;
		this.yy = yy;
	}

	show() {
		strokeWeight(10);
		stroke("rgba(0,255,90,0.5)");
		circle(this.x, this.y, 10);
		line(this.x, this.y, this.xx, this.yy);
		circle(this.xx, this.yy, 10);
	}
}

class Fulfilment {
	constructor(x, y, name) {
		this.x = x;
		this.y = y;
		this.name = name;
		this.w = 100;
		this.h = 25;
		this.selected = false;
		this.answer = false;
		this.answered = false;
	}

	show(mx, my) {
		if (this.answer) {
			//correct answer
			fill(0, 200, 0);
			rect(this.x, this.y, this.w, this.h);
			textSize(15);
			fill(255);
			text(this.name, this.x + this.w / 10, this.y + this.h - 5);
		} else {
			//incorrect answer
			if (this.answered) {
				fill(200, 0, 0);
				rect(this.x, this.y, this.w, this.h);
				textSize(15);
				fill(255);
				text(this.name, this.x + this.w / 10, this.y + this.h - 5);
			} else {
				//not yet answered
				fill(this.hover(mx, my) ? 120 : 255);
				rect(this.x, this.y, this.w, this.h);
				textSize(15);
				fill(this.hover(mx, my) ? 255 : 120);
				text(this.name, this.x + this.w / 10, this.y + this.h - 5);
			}
		}
	}

	//are we touching the box?
	hover(x, y) {
		if (x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.h) {
			return true;
		} else {
			return false;
		}
	}

	click(x, y) {
		this.hover(x, y) ? (this.selected = true) : (this.selected = false);

		//onbox, so we don't draw lines outside the boxes
		this.hover(x, y) ? (onbox = true) : (onbox = false);

		if (this.selected && sa !== null) {
			if (this.name === sa && !this.answered) {
				this.answer = true;
				this.answered = true;
				score += 1;
				sa = null;
			} else {
				if(this.answered){
					this.answer = true;
					this.answered=true;
				}
				else{
					this.answer = false;
					this.answered = false;
				}
                sa = null;
                lines.splice(lines.indexOf(lined.x2,x))
                //window.alert(lines)
			}
		}
		return this.answer;
	}
}

class Prophecy {
	constructor(x, y, name, capital) {
		this.selected = false;
		this.answered = false;
		this.matched = false;
		this.x = x;
		this.y = y;
		this.name = name;
		this.w = 350;
		this.h = 25;
	}

	show(mx, my) {
		strokeWeight(0.5);
		stroke(100, 100, 200);
		fill(this.hover(mx, my) ? 120 : 255);
		rect(this.x, this.y, this.w, this.h);
		textSize(15);
		fill(this.hover(mx, my) ? 255 : 120);
		text(this.name, this.x + 5, this.y + this.h - 5);
	}

	hover(x, y) {
		if (x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.h) {
			return true;
		} else {
			return false;
		}
	}

	click(x, y) {
		let val = "";
		this.hover(x, y) ? (this.selected = true) : (this.selected = false);
		this.hover(x, y) ? (onbox = true) : (onbox = false);
		if (this.selected) {
			val = this.name.split(" - ");
            //window.alert(data[data.findIndex(i => i.Prophecy === this.name)].Fulfilment)
			sa = data[data.findIndex(i => i.Prophecy === val[1])].Fulfilment;
			data[data.findIndex(i => i.Prophecy === val[1])].matched = true;
			//given the click, find the capital
		}
		return this.selected;
	}
}